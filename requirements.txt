snorkel==0.7.0b0
pandas==0.23.0
scikit-learn==0.22
tensorflow==1.15.0
Keras==2.3.1

numpy==1.16.0
tsfresh==0.11.2
future==0.17.1